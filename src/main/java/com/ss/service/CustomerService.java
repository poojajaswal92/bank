package com.ss.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.ss.entity.Customer;


public interface CustomerService {
	public List<Customer> getCustomers() ;
	public void saveCustomer(Customer customer);
	public Customer getCustomer(int id);
	public void deleteCustomer(int id);
	public void updateCustomer(Customer customer,int id);
}
//client =id+object of customer
//fetch data from id
//from id=DB Customer object
//set client value in db object ..only which are not null
//then call save method
