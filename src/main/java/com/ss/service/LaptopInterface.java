package com.ss.service;

import java.util.List;

import com.ss.entity.Customer;
import com.ss.entity.Laptop;

public interface LaptopInterface {
	public List<Laptop> getLaptop() ;
	public void saveLaptop(Laptop laptop);
	public void updateLaptop(Laptop laptop);

}
