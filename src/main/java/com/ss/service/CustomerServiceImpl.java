package com.ss.service;

import java.util.List;

import javax.swing.text.DefaultEditorKit.CutAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ss.dao.CustomerDao;
import com.ss.entity.Customer;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDao customerDao;
	public List<Customer> getCustomers() {

		return customerDao.findAll();
	}
	
	public void saveCustomer(Customer customer) {
		customerDao.save(customer);

	}

	public Customer getCustomer(int id) {
		return customerDao.findCustomerById(id);
	}

	public void deleteCustomer(int id) {
		customerDao.deleteById(id);

	}

	public void updateCustomer(Customer customer,int id) {
		customer.setName(customer.getName());
		customerDao.save(customer);
		
		
		
	}

	}

	
	
		
	


