package com.ss.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ss.dao.LaptopDao;
import com.ss.entity.Laptop;

@Service
public class LaptopServiceImpl implements LaptopInterface {
	@Autowired
	private LaptopDao laptopdao;

	public List<Laptop> getLaptop() {
		return laptopdao.findAll();

	}

	public void saveLaptop(Laptop laptop) {
		laptopdao.save(laptop);

	}

	public void updateLaptop(Laptop laptop) {
		laptopdao.save(laptop);

	}

}
