package com.ss.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.ss.entity.Customer;
import com.ss.service.CustomerService;
@RestController
public class AccountController {
	@Autowired
	private CustomerService customerServices;

	@GetMapping(value = "/getAll")
	public List<Customer> getUser() {
		return customerServices.getCustomers();
	}
	@GetMapping(value = "/getCustById/{id}")
	public Customer getUser(@PathVariable int id) {
		return customerServices.getCustomer(id);

	}

}