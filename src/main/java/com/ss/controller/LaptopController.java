package com.ss.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ss.entity.Laptop;
import com.ss.service.LaptopServiceImpl;

@RestController
public class LaptopController {
	@Autowired
	private LaptopServiceImpl laptopServices;
	
	@GetMapping(value="/findLaptop")
	public List<Laptop> getAllLaptop(){
		return  laptopServices.getLaptop();
		
	}
	@PostMapping(value="/saveLaptop")
	public String saveLaptop(@RequestBody Laptop laptop) {
		 laptopServices.saveLaptop(laptop);
		 return "Record has been inserted";
	}

}
