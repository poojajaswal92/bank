package com.ss.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ss.entity.Customer;
import com.ss.service.CustomerService;



@RestController
public class CustomerController {
	@Autowired
	private CustomerService customerServices;

	@GetMapping(value = "/getAll")
	public List<Customer> getUser() {
		return customerServices.getCustomers();
	}

	@GetMapping(value = "/getCustById/{id}")
	public Customer getUser(@PathVariable int id) {
		return customerServices.getCustomer(id);

	}

	@PostMapping(value = "/saveCustomer")
	public String save(@RequestBody Customer customer) {
		customerServices.saveCustomer(customer);
		return "Inserted successfully";
	}


}
