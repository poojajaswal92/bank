package com.ss.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ss.entity.Laptop;
@Repository
public interface LaptopDao extends JpaRepository<Laptop, Integer> {
	

}
