package com.ss.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ss.entity.Customer;

@Repository
public interface CustomerDao extends JpaRepository<Customer, Integer> {

	@Query("Select c from Customer c where c.id = :id")
	Customer findCustomerById(@Param("id") Integer id);
	
  
}